#Exploration

##Changes
###12/31/2011
* Started project
* Set up tree
* Added Raphaël
* Added the grass texture

###01/01/12
* Added Player and Character classes
* Added Homestead drawing function
* Fixed script path issues
* Added the test sprite

##People to Thank
* Open Game Art Users:
  * athile
  * CharlesGabriel
* Dmitry Baranovskiy for Raphaël JS